import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopbarComponent } from './component/partial/topbar/topbar.component';
import { FooterComponent } from './component/partial/footer/footer.component';
import { HomeComponent } from './component/pages/home/home.component';
import { ProductBenefitComponent } from './component/pages/product-benefit/product-benefit.component';
import { FaqsComponent } from './component/pages/faqs/faqs.component';
import { ContactUsComponent } from './component/pages/contact-us/contact-us.component';
import { LoginComponent } from './component/pages/login/login.component';
import { RegisterComponent } from './component/pages/register/register.component';
import { RegisterStep1Component } from './component/pages/register-step1/register-step1.component';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    FooterComponent,
    HomeComponent,
    ProductBenefitComponent,
    FaqsComponent,
    ContactUsComponent,
    LoginComponent,
    RegisterComponent,
    RegisterStep1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
