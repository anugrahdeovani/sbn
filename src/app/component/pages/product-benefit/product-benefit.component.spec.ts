import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductBenefitComponent } from './product-benefit.component';

describe('ProductBenefitComponent', () => {
  let component: ProductBenefitComponent;
  let fixture: ComponentFixture<ProductBenefitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductBenefitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductBenefitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
