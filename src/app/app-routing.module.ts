import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaqsComponent } from './component/pages/faqs/faqs.component';
import { ContactUsComponent } from './component/pages/contact-us/contact-us.component';
import { HomeComponent } from './component/pages/home/home.component';
import { LoginComponent } from './component/pages/login/login.component';
import { ProductBenefitComponent } from './component/pages/product-benefit/product-benefit.component';
import { RegisterStep1Component } from './component/pages/register-step1/register-step1.component';
import { RegisterComponent } from './component/pages/register/register.component';

// import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: "", component:HomeComponent },
  { path: "home", component:HomeComponent },
  { path: "product-benefit", component: ProductBenefitComponent },
  { path: "faq", component: FaqsComponent },
  
  { path: "contact-us", component: ContactUsComponent },
  { path: "web/sbn/login", component: LoginComponent },
  { path: "web/sbn/registration", component: RegisterComponent },
  { path: "web/sbn/registration-step1", component: RegisterStep1Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
